package screenudpcontroller.msdevs.com.screenudpcontroller.udp;



class ExceptionResult implements UDPResult {
    private Exception mException;

    ExceptionResult(Exception exception) {
        mException = exception;
    }

    @Override
    public String getResult() {
        return null;
    }

    @Override
    public Exception getException() {
        return mException;
    }

    @Override
    public boolean isSuccessful() {
        return false;
    }
}
