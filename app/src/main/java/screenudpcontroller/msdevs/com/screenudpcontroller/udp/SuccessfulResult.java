package screenudpcontroller.msdevs.com.screenudpcontroller.udp;



class SuccessfulResult implements UDPResult {
    private String mMessage;

    SuccessfulResult(String msg) {
        mMessage = msg;
    }

    @Override
    public String getResult() {
        return mMessage;
    }

    @Override
    public Exception getException() {
        return null;
    }

    @Override
    public boolean isSuccessful() {
        return true;
    }
}
