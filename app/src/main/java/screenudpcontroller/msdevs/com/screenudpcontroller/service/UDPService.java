package screenudpcontroller.msdevs.com.screenudpcontroller.service;

import android.app.Service;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import screenudpcontroller.msdevs.com.screenudpcontroller.AutoStartReceiver;
import screenudpcontroller.msdevs.com.screenudpcontroller.MainDeviceAdminReceiver;
import screenudpcontroller.msdevs.com.screenudpcontroller.R;
import screenudpcontroller.msdevs.com.screenudpcontroller.Utils;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.Action;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.ActionManager;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.ActionSource;
import screenudpcontroller.msdevs.com.screenudpcontroller.udp.UDPConfig;
import screenudpcontroller.msdevs.com.screenudpcontroller.udp.UDPListener;
import screenudpcontroller.msdevs.com.screenudpcontroller.udp.UDPResult;
import screenudpcontroller.msdevs.com.screenudpcontroller.udp.UDPTarget;


public class UDPService extends Service implements ActionManager.Listener {
    public static final String SEND_DATA_TO_SERVICE = "com.gvfiqst.msdevs.screenudpcontroller.UDPService.SEND_DATA";
    public static final String EXTRA_WIFI = "com.gvfiqst.msdevs.screenudpcontroller.UDPService.EXTRA_WIFI";

    public static final int NOTIFICATION_ID = 0x1;
    public static final int PORT = 4466;

    private ActionManager mActionManager;
    private ServerState mState;
    private UDPActionSource mActionSource;

    private DevicePolicyManager mDevicePolicyManager;

    private UDPBroadcastReceiver mMyReceiver;
    private UDPBinder mMyBinder;

    @Override
    public void onCreate() {
        super.onCreate();

        mState = new ServerState();
        mMyBinder = new UDPBinder();

        mDevicePolicyManager = (DevicePolicyManager) getSystemService(DEVICE_POLICY_SERVICE);

        mState.isAdminEnabled = mDevicePolicyManager.isAdminActive(MainDeviceAdminReceiver.getComponentName(this));
        mState.message = "Connecting...";

        ActionManager.Config config = ActionManager.defaltConfig(this);
        // TODO: Add your custom actions here liske so:
        /** config.registerAction("ACTION_NAME", YourCustomAction.class);*/
        // Actions must extend .action.Action class (or .action.DeviceAdminAction if it's
        // a Device Admin action for more convenience)

        mActionManager = new ActionManager(config);

        registerReceiver(
                mMyReceiver = new UDPBroadcastReceiver(),
                new IntentFilter(SEND_DATA_TO_SERVICE)
        );

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(new WifiStateReceiver(),
                    new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

        // make initial call to check wifi connection if it was established already
        sendBroadcast(new Intent(WifiStateReceiver.ACTION_INIT));
    }

    private void startUDPServer(String ip) {
        stopUDPServer();

        if (mActionSource == null || !mActionSource.isRunning()) {
            UDPTarget target = new UDPTarget(new UDPConfig(PORT, 5000, true));
            mActionSource = new UDPActionSource(target);
            mActionManager.setActionSource(mActionSource);
            mActionSource.start();

            mState.message = getString(R.string.listening) + " " + ip + ":" + PORT;
            Toast.makeText(this, mState.message, Toast.LENGTH_SHORT).show();
        } else {
            Utils.log(Utils.TAG, "UDP Server is already running", null);
        }

        mMyBinder.update();
    }

    private void stopUDPServer() {
        if (mActionSource != null) {
            mActionSource.release();
            mActionSource = null;
        }
    }

    private void updateWifi(WifiState wifiState) {
        mState.isWifiEnabled = wifiState.isWifiEnabled;

        if (wifiState.isConnectionEstablished) {
            startUDPServer(wifiState.ip);
        } else {
            stopUDPServer();

            if (mState.isWifiEnabled) {
                mState.message = "Not connected to Wifi";
            } else {
                mState.message = "Wifi is disabled";
            }

            mMyBinder.update();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mMyBinder;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent broadcastIntent = new Intent(AutoStartReceiver.ACTION_RESTART);
        sendBroadcast(broadcastIntent);

        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onAction(Action action) {
        action.perform(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mMyReceiver);
        mMyReceiver = null;

        mActionManager = null;
        mDevicePolicyManager = null;

        mMyBinder.release();
        mMyBinder = null;

        Utils.log(Utils.TAG, "UDPService.onDestroy()");
    }

    public static class ServerState implements Parcelable {
        public String message = "";
        public boolean isWifiEnabled;
        public boolean isAdminEnabled;

        private ServerState() {
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(message);
            dest.writeByte((byte) (isWifiEnabled ? 1 : 0));
            dest.writeByte((byte) (isAdminEnabled ? 1 : 0));
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<ServerState> CREATOR = new Creator<ServerState>() {
            @Override
            public ServerState createFromParcel(Parcel in) {
                ServerState ss = new ServerState();

                ss.message = in.readString();
                ss.isWifiEnabled = in.readByte() != 0;
                ss.isAdminEnabled = in.readByte() != 0;

                return ss;
            }

            @Override
            public ServerState[] newArray(int size) {
                return new ServerState[size];
            }
        };
    }

    public static class WifiState implements Parcelable {
        public boolean isWifiEnabled;
        public boolean isConnectionEstablished;
        public String ip;

        public WifiState(boolean isWifiEnabled, boolean isConnectionEstablished, String ip) {
            this.isWifiEnabled = isWifiEnabled;
            this.isConnectionEstablished = isConnectionEstablished;
            this.ip = ip;
        }

        private WifiState(Parcel in) {
            isWifiEnabled = in.readByte() != 0;
            isConnectionEstablished = in.readByte() != 0;
            ip = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeByte((byte) (isWifiEnabled ? 1 : 0));
            dest.writeByte((byte) (isConnectionEstablished ? 1 : 0));
            dest.writeString(ip);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<WifiState> CREATOR = new Creator<WifiState>() {
            @Override
            public WifiState createFromParcel(Parcel in) {
                return new WifiState(in);
            }

            @Override
            public WifiState[] newArray(int size) {
                return new WifiState[size];
            }
        };
    }

    public class UDPBinder extends Binder {
        private final List<UDPServiceUpdateListener> mList = new ArrayList<>();

        public void registerListener(UDPServiceUpdateListener l) {
            mList.add(l);

            l.onStateUpdated(mState);
        }

        public void unregisterListener(UDPServiceUpdateListener l) {
            mList.remove(l);
        }

        private void update() {
            for (UDPServiceUpdateListener l : mList) {
                l.onStateUpdated(mState);
            }
        }

        private void release() {
            mList.clear();
        }
    }

    public interface UDPServiceUpdateListener {
        void onStateUpdated(ServerState serverState);
    }

    private class UDPBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (!SEND_DATA_TO_SERVICE.equals(intent.getAction())) {
                return;
            }

            if (intent.hasExtra(EXTRA_WIFI)) {
                WifiState wifiState = intent.getParcelableExtra(EXTRA_WIFI);

                updateWifi(wifiState);
            }
        }
    }

    private static class UDPActionSource extends ActionSource {
        private UDPTarget mTarget;
        private Thread mThread;
        private Handler mHandler;

        public UDPActionSource(UDPTarget target) {
            mTarget = target;
            mHandler = new Handler(Looper.getMainLooper());
        }

        public boolean isRunning() {
            return mThread != null;
        }

        public void start() {
            mThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mTarget.start(new UDPListener() {
                        @Override
                        public void onUDPResult(final UDPResult result) {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mHandler != null) {
                                        mHandler.removeCallbacks(this);
                                    }

                                    if (result.isSuccessful()) {
                                        processAction(result.getResult());
                                    }
                                }
                            });
                        }
                    });
                }
            });

            mThread.start();
        }

        @Override
        public void release() {
            super.release();

            mThread.interrupt();
            mTarget.setCycling(false);
            mTarget.close();

            mHandler = null;
        }
    }
}
