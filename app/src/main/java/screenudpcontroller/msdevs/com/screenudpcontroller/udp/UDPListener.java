package screenudpcontroller.msdevs.com.screenudpcontroller.udp;


public interface UDPListener {
    void onUDPResult(UDPResult result);
}
