package screenudpcontroller.msdevs.com.screenudpcontroller.action;


public class ActionSource {
    private ActionManager mManager;

    void bind(ActionManager manager) {
        mManager = manager;
    }

    public void release() {
        mManager = null;
    }

    protected void processAction(String action) {
        if (mManager != null) {
            mManager.parseAction(action);
        }
    }
}
