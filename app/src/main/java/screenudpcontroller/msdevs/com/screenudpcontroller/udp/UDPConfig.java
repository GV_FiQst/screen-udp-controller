package screenudpcontroller.msdevs.com.screenudpcontroller.udp;


public class UDPConfig {
    public int port;
    public int payloadMaxSize;
    public boolean isCycling;

    public UDPConfig(int port) {
        this(port, 5000, false);
    }

    public UDPConfig(int port, int payloadMaxSize, boolean withCycling) {
        this.port = port;
        this.payloadMaxSize = payloadMaxSize;
        this.isCycling = withCycling;
    }
}
