package screenudpcontroller.msdevs.com.screenudpcontroller.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;

import screenudpcontroller.msdevs.com.screenudpcontroller.Utils;


@SuppressWarnings("ConstantConditions")
public class WifiStateReceiver extends BroadcastReceiver {
    static final String ACTION_INIT = "com.gvfiqst.msdevs.screenudpcontroller.WifiStateReceiver.INIT";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Utils.isMyServiceRunning(context, UDPService.class)) {
            context.startService(new Intent(context, UDPService.class));
        }

        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wm.isWifiEnabled()) {
            WifiInfo wifiInfo = wm.getConnectionInfo();

            if (wifiInfo.getSupplicantState() == SupplicantState.COMPLETED) {
                String ip = Formatter.formatIpAddress(wifiInfo.getIpAddress());
                sendUpdate(context, new UDPService.WifiState(true, true, ip));
            } else {
                sendUpdate(context, new UDPService.WifiState(true, false, ""));
            }
        } else {
            sendUpdate(context, new UDPService.WifiState(false, false, ""));
        }
    }

    private void sendUpdate(Context context, UDPService.WifiState wifiState) {
        context.sendBroadcast(
                new Intent(UDPService.SEND_DATA_TO_SERVICE)
                        .putExtra(UDPService.EXTRA_WIFI, wifiState)
        );
    }
}
