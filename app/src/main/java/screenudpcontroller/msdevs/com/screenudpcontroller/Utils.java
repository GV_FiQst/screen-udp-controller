package screenudpcontroller.msdevs.com.screenudpcontroller;


import android.app.ActivityManager;
import android.content.Context;
import android.os.Environment;
import android.os.Looper;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class Utils {
    public static final String TAG = "ScreenUDPController";

    public static boolean isOnMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }

    public static boolean isOnWorkerThread() {
        return !isOnMainThread();
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static void log(String key, String msg, Throwable t) {
        Log.e(key, msg, t);

        printLogs();
    }

    private static void printLogs() {
        File outputFile = new File(
                Environment.getExternalStorageDirectory(),
                "ScreenUDPController_log.txt"
        );

        try {
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }

            Runtime.getRuntime().exec(
                    "logcat -f " + outputFile.getAbsolutePath()
            );
        } catch (IOException e) {
            Log.e(TAG, "", e);
        }
    }

    public static void log(String key, String msg) {
        Log.d(key, msg);

        printLogs();
    }
}
