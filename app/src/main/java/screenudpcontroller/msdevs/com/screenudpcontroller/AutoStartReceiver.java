package screenudpcontroller.msdevs.com.screenudpcontroller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import screenudpcontroller.msdevs.com.screenudpcontroller.service.UDPService;


public class AutoStartReceiver extends BroadcastReceiver {
    public static final String ACTION_RESTART = "com.gvfiqst.msdevs.screenudpcontroller.AutoStartReceiver.RESTART";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION_RESTART.equals(intent.getAction())) {
            context.startService(new Intent(context, UDPService.class));
            return;
        }

        if (!Utils.isMyServiceRunning(context, UDPService.class)) {
            context.startService(new Intent(context, UDPService.class));
        }
    }
}
