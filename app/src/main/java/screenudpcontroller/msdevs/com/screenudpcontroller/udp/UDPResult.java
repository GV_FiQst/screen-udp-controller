package screenudpcontroller.msdevs.com.screenudpcontroller.udp;

public interface UDPResult {
    String getResult();
    Exception getException();
    boolean isSuccessful();
}


