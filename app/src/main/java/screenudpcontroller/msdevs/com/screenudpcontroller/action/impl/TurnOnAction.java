package screenudpcontroller.msdevs.com.screenudpcontroller.action.impl;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import android.widget.Toast;

import screenudpcontroller.msdevs.com.screenudpcontroller.EmptyActivity;
import screenudpcontroller.msdevs.com.screenudpcontroller.MainDeviceAdminReceiver;
import screenudpcontroller.msdevs.com.screenudpcontroller.R;
import screenudpcontroller.msdevs.com.screenudpcontroller.Utils;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.Action;

import static screenudpcontroller.msdevs.com.screenudpcontroller.Utils.TAG;


public class TurnOnAction extends Action {
    @Override
    @SuppressWarnings("ConstantConditions")
    public void perform(Context context) {
        String name = context.getString(R.string.app_name);

        ((PowerManager) context.getSystemService(Context.POWER_SERVICE))
                .newWakeLock(
                        PowerManager.SCREEN_BRIGHT_WAKE_LOCK
                                | PowerManager.FULL_WAKE_LOCK
                                | PowerManager.ACQUIRE_CAUSES_WAKEUP,
                        name
                ).acquire(1500);

        ((KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE))
                .newKeyguardLock(name)
                .disableKeyguard();

        // See the docs. There are no way of resetting password on Android N and above.
        // In those versions of API, method DevicePolicyManager.resetPassword(String, int) will
        // throw SecurityException.
        ComponentName compName = MainDeviceAdminReceiver.getComponentName(context);
        DevicePolicyManager manager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (manager.isAdminActive(compName)) {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M /*
                    || manager.isActivePasswordSufficient() */) {
                manager.resetPassword("", 0);
            }
        } else {
            Utils.log(TAG, "Device admin is disabled");
            Toast.makeText(context, "Device admin is disabled", Toast.LENGTH_LONG).show();
        }

        // Start empty transparent activity to wake lock
        context.startActivity(new Intent(context, EmptyActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    }
}
