package screenudpcontroller.msdevs.com.screenudpcontroller.action.impl;


import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.widget.Toast;

import screenudpcontroller.msdevs.com.screenudpcontroller.MainDeviceAdminReceiver;
import screenudpcontroller.msdevs.com.screenudpcontroller.Utils;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.Action;

import static screenudpcontroller.msdevs.com.screenudpcontroller.Utils.TAG;

public class TurnOffAction extends Action {

    @Override
    public void perform(Context context) {
        DevicePolicyManager manager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (manager == null) {
            return;
        }

        if (manager.isAdminActive(MainDeviceAdminReceiver.getComponentName(context))) {
            manager.lockNow();


        } else {
            Utils.log(TAG, "Can't perform DeviceAdminAction");
            Toast.makeText(context, "Device admin is disabled", Toast.LENGTH_LONG).show();
        }
    }
}
