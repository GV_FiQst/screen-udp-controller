package screenudpcontroller.msdevs.com.screenudpcontroller.udp;

import android.support.annotation.WorkerThread;

import java.io.Closeable;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import screenudpcontroller.msdevs.com.screenudpcontroller.Utils;

import static screenudpcontroller.msdevs.com.screenudpcontroller.Utils.TAG;

@WorkerThread
public class UDPTarget implements Closeable {
    private DatagramSocket mDatagramSocket;
    private UDPConfig mConfig;

    public UDPTarget(UDPConfig config) {
        mConfig = config;
        openSocket();
    }

    public void start(UDPListener listener) {
        if (Utils.isOnMainThread()) {
            close();
            throw new IllegalStateException("Can't run udp on main thread!");
        }

        do {
            try {
                receiveMessage(listener);
            } catch (Exception e) {
                mConfig.isCycling = false;
            }
        } while (mConfig.isCycling);
    }

    private void receiveMessage(UDPListener listener) throws Exception {
        String msg;
        byte[] byteArr = new byte[mConfig.payloadMaxSize];

        DatagramPacket dp = new DatagramPacket(byteArr, byteArr.length);
        try {
            mDatagramSocket.receive(dp);
            msg = new String(byteArr, 0, dp.getLength());

            Utils.log(TAG, "UDPTarget#start(UDPListener) success: " + msg);
            listener.onUDPResult(new SuccessfulResult(msg));
        } catch (Exception e) {
            Utils.log(TAG, "UDPTarget#start(UDPListener) exception: ", e);
            listener.onUDPResult(new ExceptionResult(e));

            throw e;
        }
    }

    private void openSocket() {
        try {
            close();
            mDatagramSocket = new DatagramSocket(mConfig.port);
        } catch (SocketException e) {
            Utils.log("lox", "Error opening socket: ", e);
        }
    }

    public void setCycling(boolean cycling) {
        boolean close = !cycling && mConfig.isCycling;

        mConfig.isCycling = cycling;

        if (close) {
            close();
        }
    }

    @Override
    public void close() {
        if (mDatagramSocket != null) {
            mDatagramSocket.close();
            mDatagramSocket = null;
        }
    }
}