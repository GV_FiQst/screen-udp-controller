package screenudpcontroller.msdevs.com.screenudpcontroller.action.impl;

import android.content.Context;
import android.widget.Toast;

import screenudpcontroller.msdevs.com.screenudpcontroller.action.Action;


public class ToastAction extends Action {
    @Override
    public void perform(Context context) {
        StringBuilder msg = new StringBuilder();
        for (int i = 0; i < getArgsLength(); i++) {
            msg.append(" ").append(getArg(i));
        }

        Toast.makeText(context, msg.toString().trim(), Toast.LENGTH_SHORT).show();
    }
}
