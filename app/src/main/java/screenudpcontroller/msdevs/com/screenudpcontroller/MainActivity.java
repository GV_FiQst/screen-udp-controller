package screenudpcontroller.msdevs.com.screenudpcontroller;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import screenudpcontroller.msdevs.com.screenudpcontroller.service.UDPService;


public class MainActivity extends AppCompatActivity implements UDPService.UDPServiceUpdateListener {
    private static final int RC_DEVICE_ADMIN = 0xdead;

    @BindView(R.id.text)
    TextView mTextView;

    @BindView(R.id.pb)
    ProgressBar mProgBar;

    private UDPServiceConnection mConnection;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mConnection == null) {
            mProgBar.setVisibility(View.VISIBLE);
            mTextView.setVisibility(View.GONE);

            Intent i = new Intent(this, UDPService.class);
            if (!Utils.isMyServiceRunning(this, UDPService.class)) {
                startService(i);
            }

            mConnection = new UDPServiceConnection(this);
            bindService(i, mConnection, 0);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mConnection != null) {
            mProgBar.setVisibility(View.GONE);
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText("Service is unbound");

            unbindService(mConnection);
            mConnection.release();
            mConnection = null;
        }
    }


    @Override
    public void onStateUpdated(UDPService.ServerState serverState) {
        if (!serverState.isAdminEnabled) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
                    .putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, MainDeviceAdminReceiver.getComponentName(this))
                    .putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Explanation");

            startActivity(intent);
        }

        mProgBar.setVisibility(View.GONE);
        mTextView.setVisibility(View.VISIBLE);
        mTextView.setText(serverState.message);
    }

    private static class UDPServiceConnection implements ServiceConnection {
        private MainActivity mMainActivity;
        private UDPService.UDPBinder mBinder;

        public UDPServiceConnection(MainActivity mainActivity) {
            mMainActivity = mainActivity;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBinder = ((UDPService.UDPBinder) service);
            mBinder.registerListener(mMainActivity);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            release();
        }

        public void release() {
            mBinder.unregisterListener(mMainActivity);
            mBinder = null;
            mMainActivity = null;
        }
    }


}
