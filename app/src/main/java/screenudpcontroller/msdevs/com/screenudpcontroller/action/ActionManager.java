package screenudpcontroller.msdevs.com.screenudpcontroller.action;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import screenudpcontroller.msdevs.com.screenudpcontroller.Utils;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.impl.ToastAction;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.impl.TurnOffAction;
import screenudpcontroller.msdevs.com.screenudpcontroller.action.impl.TurnOnAction;

import static screenudpcontroller.msdevs.com.screenudpcontroller.Utils.TAG;

public class ActionManager {
    private static final Object[] DEFAULTS = {
            "TOAST", ToastAction.class,
            "TURN_OFF", TurnOffAction.class,
            "TURN_ON", TurnOnAction.class
    };

    public static Config defaltConfig(Listener listener) {
        Config config = new Config(listener);

        for (int i = 0; i < DEFAULTS.length - 1; i++) {
            String key = (String) DEFAULTS[i++];

            // noinspection unchecked
            Class<? extends Action> cls = (Class<? extends Action>) DEFAULTS[i];

            config.registerAction(key, cls);
        }

        return config;
    }

    private Listener mListener;
    private Map<String, Class<? extends Action>> mActions;

    public ActionManager(Config config) {
        mListener = config.getListener();
        mActions = config.getActions();
        config.release();
    }

    public void setActionSource(ActionSource source) {
        source.bind(this);
    }

    void parseAction(String act) {
        if (act.isEmpty()) {
            return;
        }

        String[] split = act.trim().split(" ");
        String name = "";
        String[] args = new String[split.length < 2 ? 0 : split.length - 1];
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].trim();

            if (i == 0) {
                name = split[i];
            } else if (i < args.length + 1) {
                args[i - 1] = split[i];
            }
        }

        Class<? extends Action> cls = mActions.get(name);
        if (cls != null) {
            try {
                Action action = cls.newInstance();
                action.setArgs(args);

                if (mListener != null) {
                    mListener.onAction(action);
                }
            } catch (Exception e) {
                Utils.log(TAG, "Error creating action object! (" + act + "):\n", e);
            }
        }
    }

    public static final class Config {
        private Map<String, Class<? extends Action>> mActions;
        private Listener mListener;

        public Config(Listener listener) {
            mActions = new HashMap<>();
            mListener = listener;
        }

        public Config registerAction(String key, Class<? extends Action> actCls) {
            mActions.put(key, actCls);

            return this;
        }

        private Map<String, Class<? extends Action>> getActions() {
            return Collections.unmodifiableMap(mActions);
        }

        private Listener getListener() {
            return mListener;
        }

        private void release() {
            mListener = null;
            mActions = null;
        }
    }

    public interface Listener {
        void onAction(Action action);
    }
}
