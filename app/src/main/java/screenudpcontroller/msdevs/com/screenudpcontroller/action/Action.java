package screenudpcontroller.msdevs.com.screenudpcontroller.action;


import android.content.Context;

import java.util.Arrays;

import screenudpcontroller.msdevs.com.screenudpcontroller.Utils;

public abstract class Action {
    private String[] mArgs;

    void setArgs(String[] args) {
        mArgs = args;
    }

    public abstract void perform(Context context);

    protected final int getArgsLength() {
        if (mArgs == null) {
            return 0;
        }

        return mArgs.length;
    }

    protected final String getArg(int i) {
        if (mArgs == null || i >= getArgsLength() || i < 0) {
            Utils.log("lox", "Arg not found: mArgs: " + Arrays.toString(mArgs) + ", i: " + i);
            return ""; // No need for exception here;
        }

        return mArgs[i];
    }
}
